package proyecto.backend.api.controllers;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import proyecto.backend.domain.entities.PersonaNatural;
import proyecto.backend.infrastructure.services.IPersonalNaturalService;

import java.util.Set;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(path = "persona_natural")
@AllArgsConstructor
@Slf4j
public class PersonalNaturalController {
    private final IPersonalNaturalService personalNaturalService;

    @PostMapping
    public ResponseEntity<PersonaNatural> create(@RequestBody PersonaNatural request){
        var juridico = this.personalNaturalService.create(request);
        return ResponseEntity.ok(juridico);
    }

    @GetMapping
    public ResponseEntity<Page<PersonaNatural>> getAll(@RequestParam(required = false) Integer page,
                                                       @RequestParam(required = false) Integer size){
        if (page == 0) page = 0;
        if (size == 0) size = 10;
        var personal = ResponseEntity.ok(this.personalNaturalService.getAll(page,size));
        return personal;
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<PersonaNatural> getById(@PathVariable Long id){
        var updatePersona = this.personalNaturalService.getById(id);
        return updatePersona == null? ResponseEntity.noContent().build() : ResponseEntity.ok(updatePersona);
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<PersonaNatural> update(@RequestBody PersonaNatural persona,
                                                       @PathVariable Long id){
        var updatePersona = this.personalNaturalService.update(persona, id);
        return updatePersona == null? ResponseEntity.noContent().build() : ResponseEntity.ok(updatePersona);
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<PersonaNatural> delete(@PathVariable Long id){
        this.personalNaturalService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping(path = "get_by_name")
    public ResponseEntity<Set<PersonaNatural>> getByName(@RequestParam(required = false) String nombre){
        var persona = this.personalNaturalService.filter(nombre);
        return ResponseEntity.ok(persona);
    }
}

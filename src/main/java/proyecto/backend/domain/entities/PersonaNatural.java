package proyecto.backend.domain.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Entity(name = "persona_natural")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PersonaNatural implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long id_usuario_financiero;
    private String nombres;
    private String primer_apellido;
    private String segundo_apellido;
    private Integer nro_documento_identidad;
    private Integer celular;
    private String  direccion_domicilio;
}

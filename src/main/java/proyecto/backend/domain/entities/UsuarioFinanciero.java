package proyecto.backend.domain.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Entity(name = "usuario_financiero")
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class UsuarioFinanciero implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate fecha_creacion;
    private String usuario_creacion;
    private LocalDate fecha_modificacion;
    private String usuario_modificacion;
}

package proyecto.backend.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import proyecto.backend.domain.entities.PersonaNatural;

import java.util.Set;

@Repository
public interface PersonalNaturalRepository extends JpaRepository<PersonaNatural, Long> {
    @Query("select p from persona_natural p where lower(p.nombres) like lower(concat('%',:nombre,'%')) or lower(p.primer_apellido) like lower(concat('%',:nombre,'%'))")
    public Set<PersonaNatural> filter(String nombre);
}

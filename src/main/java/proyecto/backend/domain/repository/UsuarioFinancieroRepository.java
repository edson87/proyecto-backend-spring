package proyecto.backend.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import proyecto.backend.domain.entities.UsuarioFinanciero;

@Repository
public interface UsuarioFinancieroRepository extends CrudRepository<UsuarioFinanciero, Long> {
}

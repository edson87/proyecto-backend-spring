package proyecto.backend.infrastructure.services;

import org.springframework.data.domain.Page;
import proyecto.backend.domain.entities.PersonaNatural;

import java.util.Set;

public interface IPersonalNaturalService {

    public PersonaNatural create(PersonaNatural persona);
    public Page<PersonaNatural> getAll(Integer page, Integer size);
    public PersonaNatural getById(Long id);
    public PersonaNatural update(PersonaNatural persona, Long id);
    public void delete(Long id);
    public Set<PersonaNatural> filter(String nombre);
}

package proyecto.backend.infrastructure.services;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import proyecto.backend.domain.entities.PersonaNatural;
import proyecto.backend.domain.entities.UsuarioFinanciero;
import proyecto.backend.domain.repository.PersonalNaturalRepository;
import proyecto.backend.domain.repository.UsuarioFinancieroRepository;

import java.time.LocalDate;
import java.util.Set;

@Transactional
@Service
@Slf4j
@AllArgsConstructor
public class PersonalNaturalService implements IPersonalNaturalService {
    private final UsuarioFinancieroRepository usuarioFinancieroRepository;
    private final PersonalNaturalRepository personalNaturalRepository;
    @Override
    public PersonaNatural create(PersonaNatural persona) {
        var newUsuFinan = UsuarioFinanciero.builder()
                .fecha_creacion(LocalDate.now())
                .usuario_creacion(persona.getNombres())
                .fecha_modificacion(LocalDate.now())
                .usuario_modificacion(persona.getNombres())
                .build();
        var juridico = this.usuarioFinancieroRepository.save(newUsuFinan);
        persona.setId_usuario_financiero(juridico.getId());

        return this.personalNaturalRepository.save(persona);
    }

    @Override
    public Page<PersonaNatural> getAll(Integer page, Integer size) {
        PageRequest pageRequest = null;
        pageRequest = PageRequest.of(page,size);
        var juridicos = this.personalNaturalRepository.findAll(pageRequest);
        return juridicos;
    }

    @Override
    public PersonaNatural update(PersonaNatural persona, Long id) {
        var natural = this.personalNaturalRepository.findById(id).orElseThrow();
        var financiero = this.usuarioFinancieroRepository.findById(natural.getId_usuario_financiero()).orElseThrow();
        financiero.setFecha_modificacion(LocalDate.now());
        financiero.setUsuario_modificacion(persona.getNombres());
        this.usuarioFinancieroRepository.save(financiero);

        persona.setNombres(persona.getNombres());
        persona.setPrimer_apellido(persona.getPrimer_apellido());
        persona.setSegundo_apellido(persona.getSegundo_apellido());
        persona.setNro_documento_identidad(persona.getNro_documento_identidad());
        persona.setCelular(persona.getCelular());
        persona.setDireccion_domicilio(persona.getDireccion_domicilio());

        return this.personalNaturalRepository.save(persona);
    }

    @Override
    public PersonaNatural getById(Long id) {
        var natural = this.personalNaturalRepository.findById(id).orElseThrow();
        return natural;
    }

    @Override
    public void delete(Long id) {
        var natural = this.personalNaturalRepository.findById(id).orElseThrow();
        this.personalNaturalRepository.delete(natural);
    }

    @Override
    public Set<PersonaNatural> filter(String nombre) {
        var persona = this.personalNaturalRepository.filter(nombre);

        return persona;
    }
}
